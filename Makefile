.PHONY: all clean dependencies user-guides check-user-guides

all: user-guides check-user-guides artifacts

clean:
	$(MAKE) -C user-guides clean
	@rm -rf artifacts

dependencies:
	@echo "Verifying depedencies are installed ..."
	@echo "  - pandoc" && which pandoc

user-guides: dependencies
	$(MAKE) -C $@

check-user-guides: user-guides
	./check-user-guides.sh

artifacts: user-guides
	@mkdir -p "$@"
	@find ./user-guides \( -iname "*.pdf" -o -iname "*.html" -o -iname "*.docx" \) | xargs -n1 -I{} cp "{}" "$@"
	@cp ./user-guides/status-report.* "$@"
	@echo "Images are expected to be outputted to the artifacts directory. "
	@echo "If you're experiencing issues please read the user-guide/README.md#using-images. "
	@-cp -r ./imgs/ "$@"/
