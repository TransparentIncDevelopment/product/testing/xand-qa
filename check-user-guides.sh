#!/usr/bin/env bash

set -eo pipefail

EXIT_CODE=0

for f in $(find . -name "*.markdown"); do
    SEARCH_RESULTS=$(grep -E '\$[a-z\.]*\$' $f || true)
    if [ "$SEARCH_RESULTS" ]; then
	EXIT_CODE=1

	echo -e "\n\033[30;91mFound template escaping bugs in '$f'\033[0;0m\n$SEARCH_RESULTS"
    fi
done

exit $EXIT_CODE
