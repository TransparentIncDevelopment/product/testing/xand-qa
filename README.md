[[_TOC_]]

# Quality Assurance: Xand Verification and Validation

QA is largely about looking at a product from the user's perspective. When this repository was created we lacked a comprehensive strategy and mechanism for assembling user-facing documentation. Consequently, for now this repo has the source for Xand documentation and the compilation pipeline system.

Internally we test these guides by referencing them during [Simulation Game Days](https://docs.google.com/document/d/1SfW8AVYsEfCpCYsyVaiAcKMbSvHVBbEaigC9SRaOaG0/edit). The idea is to simulate planned experiences for each kind of user, ensure our software works, and that it is coherent with how we communicate with partners. 

There are differences between the Simulation Game Days that we run and what we expect our partners to run. Most of these are done for sensitivity around time and things customers will likely already have available which we'll have to simulate. A few examples are listed below, but you'll find a different set of documentation that will be supplied on the Simulation Game Days that will only add special notes to our partner documentation that will explain the differences.

Example Reasons (more should be explained in the actual sgd docs):
* Registering new dns domains for each participant in an SGD is time consuming and can cause unwanted delays.
* Creating a new account for each cloud provider requires a credit card and can be costly to make sure that they are cleaned up after.
* We may use pre-release versions of our software that will require different authentication in order to enable.

In order to write these special notes. There's a [section in our user guide README.md](user-guides/README.md#simulation-game-day-considerations).

## License

MIT OR Apache-2.0

## User Guides

We have Xand User Guides for the Validators, Members, Trust Admin, and internal network operations. As the setup procedures mature we should add more context about day-to-day usage and operation.

See [./user-guides/README.md](./user-guides/README.md) for more information on how the source files are structured and compiled.

### Requirements

To keep our User Guides [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) we use a compilation pipeline which supports templates.

You'll need
- [`pandoc 2.9.2.1`](https://github.com/jgm/pandoc/releases/tag/2.9.2.1)
- `texlive`

Run the following to install those dependencies:
```bash
apt-get update -qq && apt-get install -qq make texlive texlive-latex-extra curl
curl -L -O https://github.com/jgm/pandoc/releases/download/2.9.2.1/pandoc-2.9.2.1-1-amd64.deb
DEBIAN_FRONTEND=noninteractive sudo dpkg -i pandoc-2.9.2.1-1-amd64.deb
```

### Viewing compiled artifacts

After each merge to `master` we use [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) to publish the [latest HTML user guides](https://transparentincdevelopment.gitlab.io/product/testing/xand-qa/). 

The user guides are compiled to HTML, DOCX, and PDF during the build job of every CI pipeline run.

1. Open the desired pipeline page, e.g. [this one](https://gitlab.com/TransparentIncDevelopment/product/testing/xand-qa/-/pipelines/173851564)
1. Verify the build job is green and click it.
1. On the right under "Job Artifacts" click either "Download" or "Browse" to see the compiled artifacts.

Note that content does not reflow on PDFs as well as HTML and DOCX, so before distributing PDF copies it's a good idea to do some typesetting. For example, long lines in code blocks may flow off the right side of the page. In that example, using `\\` to split a long shell command across multiple lines helps.

## Adding Content to the User Guides

To learn more about how to contribute content to the user guides see the [README.md](user-guides/README.md) in the `user-guides` directory.

## Managing the Overall Repo

To understand more about how to mangage this repo like tagging to keep track of a specific version of the docs. See the [CONTRIBUTING.md](CONTRIBUTING.md).

## Automation Design

There's is a [AUTOMATION DESIGN document](user-guides/AUTOMATION_DESIGN.md) if you're looking to see understand the general approach for how the Makefiles build out our user-guide documents.
