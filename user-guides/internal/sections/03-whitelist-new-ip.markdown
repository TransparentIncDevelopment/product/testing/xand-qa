# Allowlisting new IPs

See [thermite/xand-api/README.md](https://gitlab.com/TransparentIncDevelopment/product/apps/thermite/-/blob/develop/xand-api/README.md)
for instructions to install `grpcurl` and `bloomrpc`.

## See existing allowlist

> This is a hack. Newer versions of the validator provide a `GetAllowlist` method on the Xand API.

With your `kubectl` connected to the cluster where the validator is running:

```bash
kubectl exec -it -n xand deployment/allowlister -c allowlist-updater -- cat /var/allowlist/allowlist.conf
```

## Allowlist CIDR block 

### Download XAND API proto definitions

Find the latest version of `xand-api-proto` published from `thermite`.

For example, download `1.0.13` with

```bash
curl -u"$ARTIFACTORY_USER:$ARTIFACTORY_PASS" https://transparentinc.jfrog.io/artifactory/artifacts-external/xand-api/xand-api-proto-1.0.13.zip
unzip xand-api-proto-1.0.13.zip -d xand-api-proto
cd xand-api-proto 
```

### Via `grpcurl`

```bash
export XAND_API_URL="<XAND_API_URL>"
export CIDR_BLOCK="<NAT_IP>/32"
export VALIDATOR_ADDRESS="<authority_public_key>"
export PROTO_PATH=./xand-api.proto

grpcurl \
    -proto "$PROTO_PATH" \
    -H "Authorization: Bearer $VALIDATOR_JWT" \
    -d "{\"issuer\": \"$VALIDATOR_ADDRESS\",\"allowlist_cidr_block\": {\"cidr_block\": \"$CIDR_BLOCK\"}}" \
    $XAND_API_URL:443 \
    xand_api.XandApi.SubmitTransaction
```

### Via bloomrpc

1. Open GUI
1. Import xand-api.proto
1. Configure the URL (Use `$XAND_API_URL` from above)
1. Add JWT auth to metdata at the bottom
    ```json
    {"Authorization": "Bearer <YOUR_JWT>"}
    ```
1. Select the `SubmitTransaction` definition
1. Alter the body in the editor to be
    ```json
    {
      "issuer": "<VALIDATOR_ADDRESS>",
      "allowlist_cidr_block": {
        "cidr_block": "<CIDR_BLOCK>"
      }
    }
    ``` 
1. Click on the **TLS** option and select "Server Certificate"
1. Click **Play**

Example:
![bloomrpc-example](images/bloomrpc-example.png)
