## Proposals

To submit a proposal, use the `propose` command (All possible actions to propose can be found in the
`propose` help menu):
```bash
network-voting-cli propose <action>
```

To view all existing proposals, use the `list` command:
```bash
network-voting-cli list
```

Or view details on a specific proposal with `get-proposal`:
```bash
network-voting-cli get-proposal <proposal_id>
```
