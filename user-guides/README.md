[[_TOC_]]
# User Guides

## Overview

Xand involves a variety of users with a mix of common and distinct concerns. 
To optimize for comprehension we maintain user guides per user type. 
For example, validators and members (should) each have their own user guide.

## Building
These user guides use [pandoc](https://pandoc.org) to convert source code to documents in end-user-friendly formats, such as PDF.

To output content:

```bash
make
```

To clean up generated docs:

```bash
make clean
```

## Working in this repo

The Makefiles expect a particular directory structure, which is illustrated in [./example](./example).

```
$ tree example/
example/
├── example.pdf
├── Makefile
├── sections
│   ├── 01-introduction.markdown
│   ├── 02-example-common-section.markdown -> ../../common/example-common-section.markdown
│   └── 03-example-section-with-subsections
│       ├── 00-section.markdown
│       ├── 01-first-subsection.markdown
│       └── 02-second-subsection.markdown
└── title.txt
```

`./example/example.pdf` was created by running `make` in either this directory or the root directory of the repository. Running `make clean` should remove it.

The `*.markdown` files in the `./example/sections` directory tree are processed by [pandoc](https://pandoc.org), which supports [more features](https://pandoc.org/MANUAL.html#pandocs-markdown) than [CommonMark markdown](https://commonmark.org/). For example, [heading identifiers](https://pandoc.org/MANUAL.html#heading-identifiers) support linking to other sections of the document.

## Status tracking

See statuses to track at the end of the acceptance critera in [this feature](https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/4468).

Each `*.markdown` file should include a [YAML metadata block](https://pandoc.org/MANUAL.html#extension-yaml_metadata_block) which indicates that user guide section's status. 
For example, `./example/section/01-introduction.markdown` begins

```
---
status: todo
...
```

The following statuses are recommended to help track progress.

1. todo
1. blocked on ...
1. ready for verification
1. last sanity checked at <revision> on <date>
1. last verified at <revision> on <date>

By default when the user guides are compiled using `make` a `status-report.md` file is generated with a table of the statuses for each audience and section.

## Creating a new User Guide

1. Copy the `./example` directory to another subdirectory named according to the target audience, e.g. `./validators`.
1. Open `./Makefile` and add the new subdirectory to the `AUDIENCE_DIRS` list. Now running `make` should create a new PDF in that subdirectory.
1. Update metadata in `./$AUDIENCE/title.txt`.
1. Revise and add content in `./$AUDIENCE/sections/`. The sorted order of filenames in this directory determines how they are sequenced in the PDF.
1. Keep in mind that files in the sections directory may be symbolically linked to the `./common` directory, and may be incorporated into other user guides.


### Creating links
To create a symbolic link to a section of content:

```
pushd ./$AUDIENCE/sections
ln -s ../../common/example-common-section.markdown ./04-common-section-reference.markdown
popd
```

## Style conventions

Users may not be very experienced with the Linux command line and `bash`, but these environments are fundamental to the manual setup process. Adhering to style conventions should help reduce the risk of user error.

- Sequences of `bash` commands should be enclosed in a fenced code block annotated with `bash`.
- Interactive `bash` commands should not be prefixed with a command line prompt such as `$` or `#`.
- Environment variable assignments in interactive commands should be `export`-ed, e.g. `export ARTIFACTORY_USER="<your-artifactory-account>"`.
  - If you need help locating your Artifactory account, see [this Confluence link](https://transparentinc.atlassian.net/wiki/spaces/PD/pages/1962311681/Utilizing+Artifactory+with+RBAC) and if necessary, reach out to one of the admins listed on that page.
- Variable assignments added to `*.env` files should _not_ be prefixed with `export`.
- When the user must provide input, say so in the prose and follow it with a code block with lines like, `export COMPANY_NAME=<INSERT YOUR COMPANY NAME>`. To reduce copy-paste confusion, avoid adding other kinds of lines to the same code block.
- As time permits, favor shipping executable code over requiring the user to manually execute many shell commands.
- `bash` commands inside Pandoc templates should escape `$` characters, e.g. `echo $$SOME_ENV_VAR`

## Re-using content
Some links between content may already exist. To view them, see [Working in this Repo](#working-in-this-repo)

### Re-using common content

The user guides use symlinking to re-use content that is standard across user guides. 
See [Creating links](#creating-links) for examples and instructions.

To inject a common piece of content into a template, add a symlink to the common template, from the reference location in a user guide.

A truncated example:

```bash
tree
.
├── .templates
│   ├── generate-key.markdown.template 
├── common
│   ├── keygen-intro.markdown
│   ├── preliminary-setup
│   │   ├── 01-setup-docker.markdown
│   │   ├── 01-setup-docker.markdown.template
│   │   └── 01-setup-docker.markdown.yaml
├── members
│   ├── sections
│   │   ├── 00-preliminary-setup -> ../../common/preliminary-setup/
│   │   ├── 02-configure-xand-node
│   │   │   ├── 01-keygen-intro.markdown -> ../../../common/keygen-intro.markdown
│   │   │   ├── 02-generate-xand-key.markdown.template -> ../../../.templates/generate-key.markdown.template
```

In the example above, we have:

* Linked a whole directory: `00-preliminary-setup -> ../../common/preliminary-setup/`

> The contents of the `./common/preliminary-setup/` 
directory will be rendered in place of the `./members/sections/00-preliminary-setup` directory.


* Linked a markdown file: `01-keygen-intro.markdown -> ../../../common/keygen-intro.markdown`
>The un-templated `./common/keygen-intro.markdown` 
file will be rendered at `./members/sections/02-configure-xand-node`.

* Linked a template: `02-generate-xand-key.markdown.template -> ../../../.templates/generate-key.markdown.template/01-keygen-intro.markdown` file.

> The templated results of the `./.templates/generate-key.markdown.template` 
file will be rendered at `./members/sections/02-configure-xand-node/02-generate-xand-key.markdown.template`, 
the contents of which will then output to the `./members/sections/02-configure-xand-node/02-generate-xand-key.markdown` file. 

## Using templates

This repo uses `*.template` files to generate output files according to pandoc [templating](https://pandoc.org/MANUAL.html#templates). 

For example, a `*.markdown.template` file can output to a corresponding `*.markdown` file.

>Note: Some *.markdown files are not templated; they do not have a *.markdown.template corollary, and will appear in the outputted docs as-is. 

### Variable interpolation

Any `*.markdown.template` file that uses variable interpolation should include them in 
[YAML notation](https://pandoc.org/MANUAL.html#interpolated-variables) in an associated `*.markdown.yaml` file.
For example, for  `./example/section/02-example-common-section.markdown.yaml`:

```yaml
entity:
  name: Trust
  alt_name: trustee
  key_type: trust
```

The `*.markdown` snippet `This is content about the $entity.name$.` will be interpolated as `This is content about the Trust.` in the published version of the guide. 
See the example [here](example/sections/02-example-common-section.markdown.template).

#### Conditionals

Templates can be rendered conditionally based on variables. See [Pandoc conditional usage](https://pandoc.org/MANUAL.html#conditionals).

The implementation of this functionality is a little tricky. 

**To simplify usage, this repo should use the following convention:**

To include a section in a conditional template, 
set the relevant variable in the corresponding `*.markdown.yaml` file to `DEFINED`.

```yaml
entity:
  name: Validator
  has_encryption_pub_key: DEFINED
```

To exclude a section, comment out the variable.

```yaml
entity:
  name: Validator
#  has_encryption_pub_key: DEFINED
```

### Simulation Game Day Considerations

This repository generates documentation for two different audiences: our customers and Simulation Game Days (SGD) participants. SGD participants require additional information, so the markdown templates contain special "Notes" sections for SGD participants that only appear in SGD flavors of the guides. These note sections highlight SGD differences, while still giving developers an opportunity to see what customers see.

Here's the format to use when specifying an SGD notes section.

```markdown
$if(show_sgd_notes)$
---

<span style="color: darkred;">
**SGD Instructions**
</span>

<span style="color: darkred;">
My super special instructions here.
</span>

---

$endif$
```

**Note the space before the endif as it will make sure there's a space when rendered and no extra space when it's not rendered.**

## Using Images

If you'd like to include images in your document they are relative to where pandoc runs which is where
the Makefile for the particular user doc. So the example that'll be used here is the internal user
guide. `user-guides/internal/` has it's Makefile located at `user-guides/internal/Makefile`.

1. Let's create an `images` directory at this path if it doesn't already exist `user-guides/internal/images`.
1. Include a `screenshot.png` file here to be `user-guides/internal/images/screenshot.png`.
1. Then include the path `images/screenshot.png` in your markdown. `![The Swankest Screenshot](images/screenshot.png)`
1. To confirm that this is working correctly run `make` in `user-guides/internal/`. Assuming that you've
   installed the requirements from [User Guide Requirements](../README.md#requirements) to run pandoc.
1. Open the resulting `internal.pdf`, `internal.html` and `internal.docx` to confirm that they contain
   your image or run `make artifacts` from the project directory and check the same results in the
   `artifacts` directory. Lastly, you can also check the results from the gitlab ci build artifacts
   results.

Unfortunately there is a known [issue](https://github.com/jgm/pandoc/issues/3752  
) that images won't render in a Gitlab preview on that markdown page at the moment or local markdown
previews that exists in some editors.  
As described, I attempted to use resourcepath, but didn't work with ".." so it means the images would
have to be consolidated to work. I opted for keeping images directory separate for each user guide.

## Automation Design

There's is a [AUTOMATION DESIGN document](AUTOMATION_DESIGN.md) if you're looking to see understand the general approach for how the Makefiles build out our documents.
