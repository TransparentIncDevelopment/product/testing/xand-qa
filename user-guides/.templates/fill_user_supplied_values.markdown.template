$if(show_sgd_notes)$
---

<span style="color: darkred;">
**SGD Notes for Fill Out the User Supplied Values File**
</span>

<span style="color: darkred;">
The `Ansible Vars` tab in the excel sheet has been filled out to help make the process of filling out this part of the configuration files easier.
Please go to column `D` which will go left to right for the values to the variables within this file.
You should see a header of "User Supplied Values" which highlights all values that need to be filled out in this file.
Please make sure to get to the last column which means that you should be done filling out this file.
</span>

---

$endif$
# Fill Out the User Supplied Values File

Open the `vars/user_supplied_values.yml` file in your favorite text editor and fill out the values as
described below.

Put your company name here. It must **NOT** contain spaces.
```yaml
# Company Information
company_name: "my_company_inc"
```

This will configure the cloud provider targeted for provisioning and deployment. Only Amazon AWS, Google Gcloud,
and Microsoft Azure are supported. Please enter your provider here ("google", "aws", "azure"). This is an example for
Amazon:
```yaml
# Cloud Provider
# Specify cloud provider for deployments e.g. "google", "aws", "azure"
cloud_provider: "aws"
```

Set the xand-api domain name you wish to use. It should be on a domain controlled by you, and the record does not
need to exist yet.
```yaml
xand_api_domain_name: "xand-api.mydomain.com"
```

$if(validator)$
Next, set the peering domain name you wish to use.
```yaml
peering_domain_name: "peering.mydomain.com"
```
$endif$

$if(member)$
Next, set the member-api domain name you wish to use.
```yaml
member_api_domain_name: "member-api.mydomain.com"
```
$endif$

$if(trust)$
Next, set the trust-api domain name you wish to use.
```yaml
trust_api_domain_name: "trust-api.mydomain.com"
```
$endif$

This will configure whether telemetry is sent to Transparent Systems.
```yaml
# Telemetry - To disable telemetry being sent to Transparent Systems, this variable must be set to "no" or "false".
send_transparent_systems_logging_and_metrics: yes
```

A Promtheus and Grafana instance are deployed to allow you to see the health of your your own components. Fill out the
prometheus and grafana domain name you wish to use.
```yaml
prometheus_domain_name: "prometheus.mydomain.com"
grafana_domain_name: "monitoring.mydomain.com"
```
