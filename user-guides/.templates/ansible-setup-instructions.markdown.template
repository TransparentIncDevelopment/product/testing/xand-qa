# Setup Ansible

$if(show_sgd_notes)$
---

<span style="color: darkred;">
**SGD Notes for Setup of Ansible**
</span>

<span style="color: darkred;">
The Artifactory Credentials below can be found in the `Credentials` tab of the excel sheet or use your personal credentials.
</span>


<span style="color: darkred;">
If you are testing a non-public release, please make sure your path contains `artifacts-internal`; otherwise it can remain `artifacts-external`.
</span>
```bash
curl --fail -u"$${ARTIFACTORY_USER}":"$${ARTIFACTORY_PASS}" \
  -LO https://transparentinc.jfrog.io/artifactory/artifacts-internal/ansible/$partner_type/lowercase$.$ansible.version$.zip
```
---

$endif$

Let start by creating a directory for where you would like to download files to install the Xand Platform.

```bash
mkdir ~/xand_deployment
cd ~/xand_deployment
```

## Download Ansible Zip

Set the Artifactory credentials provided by Transparent Systems.

```bash
export ARTIFACTORY_USER=<INSERT USERNAME HERE>
export ARTIFACTORY_PASS=<INSERT PASSWORD HERE>
```
Next, please download the Ansible zip file that you will be working from.

```bash
curl --fail -u"$${ARTIFACTORY_USER}":"$${ARTIFACTORY_PASS}" \
  -LO https://transparentinc.jfrog.io/artifactory/artifacts-external/ansible/$partner_type/lowercase$.$ansible.version$.zip

unzip -d $partner_type/lowercase$ $partner_type/lowercase$.$ansible.version$.zip
cd $partner_type/lowercase$
```

We'll work in this directory `~/xand_deployment/$partner_type/lowercase$` from here on out.

Later you'll be asked to edit configuration files in `~/xand_deployment/$partner_type/lowercase$/vars` please have an editor of your choice available to
fill out configuration files.

## Install Python Packages and Requirements

As long as you have python installed appropriately from the step before. You should be able to run the following script:

```bash
sudo ./install_python_prereqs.sh
```

## Install Ansible Collections

If the python packages were installed appropriately then you should be able to run the following:

```bash
ansible-galaxy install -fr requirements.yml
```
