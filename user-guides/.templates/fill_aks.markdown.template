# (Azure) Fill out the aks.yml file

$if(show_sgd_notes)$
---

<span style="color: darkred;">
**SGD Notes for Azure**
</span>

<span style="color: darkred;">
You **do not** need to run `create_azure_service_principal.sh` during SGD.  Any values the script provides will be given to you by the SGD organizer(s).
</span>

---

$endif$

If you're deploying to Azure, use the following instructions:

## Service Principal

When deploying to Azure, a service principal must be created with owner permissions to the target subscription. For convenience, a script has been included in the `/scripts` folder called `create_azure_service_principal.sh` that will create such a service principal. When run, it will create the service principal, and then generate or update a file called `aks.yml` in the Ansible `/vars` folder with the credentials for the service principal.

```bash
./scripts/create_azure_service_principal.sh
```

The following information in `vars/aks.yml` will be automatically filled out by this script. If you are using the script, please skip ahead to the Additional Variables section. If you are [creating the service principal yourself](https://docs.microsoft.com/en-us/azure/active-directory/develop/howto-create-service-principal-portal), you'll need to fill out these variables by hand.

subscription_id: Which Azure subscriptions region do you want to deploy to.

```yaml
  subscription_id: "00000000-0000-0000-0000-000000000000"
```

client_id: This is the Client Id, sometimes called an App Id, for the service principal that will be used to deploy the infrastructure.

```yaml
  client_id: "00000000-0000-0000-0000-000000000000"
```

client_secret: This is the Tenant Secret, sometimes called a Tenant Password, for the service principal that will be used to deploy the infrastructure.

```yaml
  client_secret: "00000000000000000000000000000"
```

tenant_id: This is the Tenant ID for the service principal that will be used to deploy the infrastructure.

```yaml
  tenant_id: "00000000-0000-0000-0000-000000000000"
```

## Additional Variables

The following variables need to be filled out before running a playbook.

resource_group: Choose a name for a group that will be created where resources are collected together for the xand platform.

```yaml
  resource_group: "MyResourceGroup"
```

storage_account: Choose a name for a storage account that will be created for storing the terraform state.

```yaml
  storage_account: "MyStorageAccount"
```

storage_container: Choose a name for a storage container that will be created for storing the terraform state.

```yaml
  storage_container: "MyStorageContainer"
```

region: The Azure region you wish to deploy to. (Choose whatever is closest to your end users)

```yaml
  region: "eastus"
```

cluster_name: Choose a cluster name for your AKS kubernetes cluster.

```yaml
  cluster_name: "MyCluster"
```

### Deployment Variables

The following variables included in the template file can be left as-is -- they will be filled in automatically during
deployment:

* `resource_name_prefix`
* `node_min_count`
* `node_max_count`
