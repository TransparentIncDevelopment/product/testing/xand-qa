# Fill out trust configuration

## Bank Configuration

The sections below describe the different bank details that will need to be filled out in the `trust.yml` variable file for each bank for how the trust software will interact with each bank.

Open the `vars/trust.yml` file in your favorite text editor, and fill out the following variables.

### Metropolitan Commercial Bank

This is your (the trust) bank account number at MCB. Put the MCB trust account here:
```yaml
mcb_trust_account: "01010203"
```
Metropolitan will provide you with the URL base to their Bank API. Put it here as this example shows:
```yaml
mcb_url: "https://api-example.mcb.com/"
```
Update Metropolitan's Bank routing number if needed here:
```yaml
mcb_routing_number: "121141343"
```

### Provident Bank

This is your (the trust) bank account number at Provident. Put the Provident trust account here:

```yaml
provident_trust_account: "01010203"
```

Provident will provide you with the URL base to their Bank API. Put it here as this example shows:
```yaml
provident_url: "https://api-example.provident.com/"
```

This is the Provident Bank routing number.

```yaml
provident_routing_number: "211374020"
```

### Bank Account Allowlisting

In order for an added layer of protection. The trust software will only allow interaction with bank accounts that have been
allowlisted. This section is where you will specify each allowlisting.

```yaml
member_bank_account_allowlist:
```

Underneath this specification you will want to specify each allowlisting for each bank account with the following syntax.

```yaml
  example-member-name:
    - account_number: '2000000'
      routing_number: '121141343'
    - account_number: '3000000'
      routing_number: '211374020'
```
