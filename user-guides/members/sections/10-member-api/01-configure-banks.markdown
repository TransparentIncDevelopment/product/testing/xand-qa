# XAND Bank Configuration

Adding bank accounts for use on the XAND network to create and redeem claims will require some additional configuration.

## Adding a Bank

For each bank you'd like to use you'll need to add them to the Member API. You can do this via the API docs hosted at the root path of your member-api (https://member-api.mycompany.com/) or by issuing curl requests.

We currently provide support for accounts at banks that use the following APIs:

* Metropolitan Commercial Bank (used by MCB)
* Treasury Prime (used by Provident (BankProv) Bank)

### Configuration Details

#### Add/Remove workflow

When adding accounts, you'll first need to have their bank configured. This will give you the `bankId` which will allow you to associate the accounts with the correct bank.

When removing banks, you'll first need to remove all the accounts held at that bank. Once a bank is free from accounts, it can be deleted.

We will explain how to do both of these interactions with both supported methods in subsequent sections, but regardless of your approach the order will need to stay the same. 

#### Bank Configuration

Each bank you add, regardless of which institution it is, will require the following fields:

* `name` - this can be the bank name or a nickname, it's just used for display purposes.
* `routingNumber` - the actual routing number for the bank.
* `trustAccount` - provided by Transparent, this is the account number for the trust at this bank. The Member API transfers funds to the Trust account when creating XAND claims.

##### A Note on Bank Adapters and Authentication

Both the MCB and Treasury Prime bank adapters require authentication information. For both banks, they need a `url` to send bank requests to.

The remaining fields involve authentication secrets which should have been set during your member-api setup. If you need to change these values, edit your `secrets.yaml` file and re-deploy the member-api. You won't lose any configured banks.

For each of the `secret` fields at either bank, you'll be providing a lookup key of where that secret is located in your `secrets.yaml` file.

* If you didn't change the keys while setting up, the example values can be left as-is. 
* If you changed those keys you'll need to update the values for each request field appropriately.

If the secrets aren't correctly configured your Member API will be unable to communicate with the affected bank. Please reach out to your TPFS rep if you need additional assistance finding and setting these values -- they can be tricky to configure but are essential to using your bank accounts to create and redeem claims.

#### Account Configuration

Accounts have three fields:

* `bankId` - the `id` of the bank that you're adding this account to. You'll get this value when you configure the bank.
* `shortName` - a nickname for the account. This field is used for display purposes.
* `accountNumber` - the bank account's number, provided by your bank.

### Using the API Documentation

To add banks and accounts through the API docs UI you'll need a web browser.

In your browser of choice, navigate to the root path of your member-api server (https://member-api.mycompany.com/). You should see a graphical representation of all of the routes provided with your member-api. 

If you're set up to use JWT authentication, the first step for using the UI will be to click the `Authorize` button in the top-right corner and entering your created JWT. This can be the same token that you've generated for using the wallet. Simply paste the value into the box and click `Authorize` -- you'll notice that all of the "unlocked" locks next to the routes are now "locked" and you'll be able to start issuing requests. If you'd like to double-check you can issue a simple `GET` request to something like the `/member` endpoint and confirm you do not get an authorization error. 

![Authorization](images/authorize.gif)

#### Adding Banks

Adding a bank uses the `POST /banks` endpoint. Navigate to that section and click on the bar to display a drop-down.

You can select which bank you'd like to add in the  `Request Body` section, either MCB or Provident (BankProv) Bank. When you see the appropriate example you can click `Try it out` to edit the individual fields.

Once you've overwritten any applicable example values, you can click the `Execute` button to send your request to the server. 

![Adding a Bank](images/add-bank.gif)

You should get a successful response showing the bank you just added with a new `id` field (you'll need this for adding accounts to it later). 

If you get a 400 error, you should make sure you haven't already entered a bank with the same name or routing number. You can do that by executing a GET /banks request and checking which banks you already have configured.

#### Adding Accounts

Now that you've added a bank you can start adding accounts. Using the `id` you got when you added a bank, you can fill out and execute a POST /accounts request. Just like when you added a bank, you'll need to navigate to the POST /accounts section, click it to open a drop-down and click the `Try it out` button to bring up an editable text area.

Using the bank's id for `bankId`, update the account's nickname and actual account number and press `Execute`. 

You should again get a successful response with the account name you just entered, an account `id` (used to identify this account for retrieval, updates, and deletes), the name, id and routing number of its associated bank, and a masked version of your account number which only displays the last four digits.

If you've gotten a 400 error, you should again check that you haven't already added an account with this account number for this bank and that the bank exists. You can do this by executing requests through the GET /accounts and GET /banks endpoints. 

#### Updating and Deleting Banks and Accounts

You can use the API docs UI to update banks and accounts through their respective PUT endpoints. You can also use the DELETE endpoints to remove accounts and banks by their id -- keep in mind that a bank cannot be removed until all of its associated accounts have been removed.

### Using cURL Requests

To use these methods you'll need to have cURL installed.

Set environment variables for the API url and JWT Authentication:

```bash
export MEMBER_API_URL="https://member-api.mycompany.com" # Note: no trailing '/'

export AUTH="Authorization: Bearer ${JWT_TOKEN_WALLET}"
```

#### Adding Banks with cURL
1. First you'll create a `bank.json` file with the bank you'd like to add. Keep in mind that the values specified are only keys referring to your `secrets.yaml` file, and not the secrets themselves.

    If you're adding MCB, it should look like the following (filled in with your data):
    
    ```bash
    {
      "name": "Metropolitan Commercial Bank",
      "routingNumber": "121141343",
      "trustAccount": "9876543210",
      "adapter": {
        "mcb": {
          "url": "https://api.mcb.com",
          "secretUserName": "mcb-username",
          "secretPassword": "mcb-password",
          "secretClientAppIdent": "mcb-client-app-ident",
          "secretOrganizationId": "mcb-organization-id"
        }
      }
    }
    ```
    
    If you're adding Provident (BankProv) Bank, it will look like this instead (again, replacing the example data as needed):
    
    ```bash
    {
      "name": "Provident Bank",
      "routingNumber": "211374020",
      "trustAccount": "9876543210",
      "adapter": {
        "treasuryPrime": {
          "url": "https://api.provident-bank.com",
          "secretApiKeyId": "provident-api-key-id",
          "secretApiKeyValue": "provident-api-key-value"
        }
      }
    }
    ```

1. Once you've created your bank.json file, you can issue the following curl POST request to add it to the member-api:

    ```bash
    curl --fail -X POST $MEMBER_API_URL/api/v1/banks \
         -H  "accept: application/json" \
         -H  "Content-Type: application/json" \
         -H  "$AUTH" \
         -d @bank.json
    ```

    If successful, you should receive a response identical to the contents of your `bank.json` file with an additional `id` parameter -- you'll need this to add accounts to your newly configured bank in a later step.
    
    If you get a 400 error response you should check that you don't already have a bank configured with the same name or routing number. You can see what banks you have configured by issuing [a GET request](#get-banks).

If you'd like to add another bank you can repeat this process, either overwriting the contents of `bank.json` or creating a new file and providing that to the POST request in the second step.

#### Adding Accounts with cURL

Once you've added a bank you can start adding accounts.

1. Create an `account.json` file and fill in your information. For an account at either bank it will look like the following:

    ```bash
   {
     "bankId": 0,
     "shortName": "Discretionary Funds",
     "accountNumber": "1234567890"
   }
    ```
   * The `bankId` field needs to match the `id` field returned when you added the bank that this account is held at. If you need to get the bank's id you can use the [GET banks request](#get-banks) -- just use the `id` field returned with the corresponding bank.
   
1. Next you'll need to issue another POST request:

    ```bash
    curl --fail -X POST $MEMBER_API_URL/api/v1/accounts \
         -H  "accept: application/json" \
         -H  "Content-Type: application/json" \
         -H  "$AUTH" \
         -d @account.json
    ```
   
   If this operation was successful you should get a response similar to the contents of your `accounts.json` file with an added `id` field, a `maskedAccountNumber` obscuring all but the last four digits of your account number, a `bankName` of the associated bank, and a `bankId` of the associated bank. 

   If you instead get a 400 error you should make sure you haven't already added an identical account for that bank. You can do this by [viewing all of the accounts you currently have configured](#get-accounts).

If you'd like to add more accounts you can repeat the previous steps, overwriting the values in `account.json` or creating a new file and providing that to the POST request in the second step.

#### Removing Banks and Accounts with cURL

##### Removing accounts

To remove an account, you'll need the account id. If you don't know it you can retrieve it with [this GET request](#get-accounts). 

You can then issue the following request to DELETE it:

```bash
export ACCOUNT_ID="id of account to delete"

curl -X DELETE $MEMBER_API_URL/api/v1/accounts/$ACCOUNT_ID -H  "accept: */*" -H "$AUTH"
```

Optionally, to confirm that the account has been removed you can again use the command to [GET accounts](#get-accounts). 

##### Removing Banks

Once all accounts at a bank have been removed you can remove the bank itself. You'll need the bank's `id` which you can get by [retrieving your configured banks](#get-banks).

When you've got the id for the bank you'd like to delete you can make the following request to DELETE it:

```bash
export BANK_ID="id of account to delete"

curl -X DELETE $MEMBER_API_URL/api/v1/banks/$BANK_ID -H  "accept: */*" -H "$AUTH"
```

If you'd like, you can confirm that the bank was removed by [listing your configured banks](#get-banks) and making sure it is no longer listed.

#### cURL Verification Requests

##### GET banks

To get a list of all configured banks you can issue the following request:

```bash
curl --fail -X GET $MEMBER_API_URL/api/v1/banks -H  "accept: application/json" -H "$AUTH" | jq
```

##### GET accounts

To view all of your configured accounts, issue the following:

```bash
curl --fail -X GET $MEMBER_API_URL/api/v1/accounts -H  "accept: application/json" -H  "$AUTH" | jq
``` 
