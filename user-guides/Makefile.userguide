# Finds all .markdown.template
SECTION_TEMPLATES = $(wildcard sections/*/*.markdown.template) $(wildcard sections/*.markdown.template)
# Converts to .markdown
SECTION_TEMPLATE_TARGETS = $(patsubst %.markdown.template, %.markdown, $(SECTION_TEMPLATES))
# Converts to sgd.markdown
SECTION_TEMPLATE_SGD_TARGETS = $(patsubst %.markdown.template, %.sgd.markdown, $(SECTION_TEMPLATES))
# Grabs all existing markdown files
JUST_SECTION_MARKDOWN_FILES := $(wildcard sections/*/*.markdown) $(wildcard sections/*.markdown)
# Exclude previous run generated markdown
JUST_SECTION_MARKDOWN_FILES := $(filter-out $(SECTION_TEMPLATE_TARGETS), $(JUST_SECTION_MARKDOWN_FILES))
JUST_SECTION_MARKDOWN_FILES := $(filter-out $(SECTION_TEMPLATE_SGD_TARGETS), $(JUST_SECTION_MARKDOWN_FILES))

SECTIONS = $(sort $(JUST_SECTION_MARKDOWN_FILES) $(SECTION_TEMPLATE_TARGETS))
SGD_SECTIONS = $(sort $(JUST_SECTION_MARKDOWN_FILES) $(SECTION_TEMPLATE_SGD_TARGETS))
AUDIENCE = $(shell basename $(shell pwd))

DEFAULT_METADATA_YAML_FILE_ARGS := --metadata-file="../global-config.yaml"
ifneq (,$(wildcard ./partner-config.yaml))
      DEFAULT_METADATA_YAML_FILE_ARGS += --metadata-file="./partner-config.yaml"
endif

.PHONY: all clean

define convert-template-to-markdown =
@pandoc \
	-o "$(OUTPUT)" \
	--highlight=zenburn \
	--template="$(INPUT)" \
	--to=markdown+fancy_lists \
        --from=markdown+fancy_lists \
	$(METADATA_YAML_FILE_ARGS) \
	/dev/null;
endef

all: $(AUDIENCE).pdf $(AUDIENCE).sgd.pdf $(AUDIENCE).html $(AUDIENCE).sgd.html $(AUDIENCE).docx $(AUDIENCE).sgd.docx status-report-rows.md

clean:
	@rm -f $(SECTION_TEMPLATE_TARGETS)
	@rm -f $(SECTION_TEMPLATE_SGD_TARGETS)
	@rm -f "$(AUDIENCE).pdf"
	@rm -f "$(AUDIENCE).html"
	@rm -f "$(AUDIENCE).docx"
	@rm -f "status-report-rows.md"

# 1. non-sgd and has a yaml file associated with template
sections/%.markdown: sections/%.markdown.template sections/%.markdown.yaml
	$(eval METADATA_YAML_FILE_ARGS = $(DEFAULT_METADATA_YAML_FILE_ARGS) --metadata-file="sections/$*.markdown.yaml")
	$(eval INPUT = "sections/$*.markdown.template")
	$(eval OUTPUT = "$@")
	$(convert-template-to-markdown)

# 2. non-sgd and no yaml file associated with template
sections/%.markdown: sections/%.markdown.template
	$(eval METADATA_YAML_FILE_ARGS = $(DEFAULT_METADATA_YAML_FILE_ARGS))
	$(eval INPUT = "sections/$*.markdown.template")
	$(eval OUTPUT = "$@")
	$(convert-template-to-markdown)

# 3. sgd and has a yaml file associated with template
sections/%.sgd.markdown: sections/%.markdown.template sections/%.markdown.yaml
	$(eval METADATA_YAML_FILE_ARGS = $(DEFAULT_METADATA_YAML_FILE_ARGS) \
					--metadata-file="../sgd-config.yaml" \
					--metadata-file="sections/$*.markdown.yaml")
	$(eval INPUT = "sections/$*.markdown.template")
	$(eval OUTPUT = "$@")
	$(convert-template-to-markdown)

# 4. sgd and no yaml file associated with template
sections/%.sgd.markdown: sections/%.markdown.template
	$(eval METADATA_YAML_FILE_ARGS = $(DEFAULT_METADATA_YAML_FILE_ARGS) --metadata-file="../sgd-config.yaml")
	$(eval INPUT = sections/$*.markdown.template)
	$(eval OUTPUT = $@)
	$(convert-template-to-markdown)

$(AUDIENCE).pdf: $(SECTIONS)
	pandoc -o "$@" --toc \
		--highlight=zenburn \
		--template=../.templates/latex.template \
		--metadata-file=../.templates/latex.template.yaml \
		title.txt $(SECTIONS)

$(AUDIENCE).sgd.pdf: $(SGD_SECTIONS)
	pandoc -o "$@" --toc \
		--highlight=zenburn \
		--template=../.templates/latex.template \
		--metadata-file=../.templates/latex.template.yaml \
		title.txt $(SGD_SECTIONS)

$(AUDIENCE).html: $(SECTIONS)
	pandoc -o "$@" --toc \
		--highlight=zenburn \
		--template=../.templates/html.template \
	        --extract-media="./imgs" \
		title.txt $(SECTIONS)
	@mkdir -p ../../imgs/
	@-cp ./imgs/* ../../imgs/

$(AUDIENCE).sgd.html: $(SGD_SECTIONS)
	pandoc -o "$@" --toc \
		--highlight=zenburn \
		--template=../.templates/html.template \
	        --extract-media="./imgs" \
		title.txt $(SGD_SECTIONS)
	@mkdir -p ../../imgs/
	@-cp ./imgs/* ../../imgs/

$(AUDIENCE).docx: $(SECTIONS)
	pandoc -o "$@" --toc \
		--highlight=zenburn \
		title.txt $(SECTIONS)

$(AUDIENCE).sgd.docx: $(SGD_SECTIONS)
	pandoc -o "$@" --toc \
		--highlight=zenburn \
		title.txt $(SGD_SECTIONS)

status-report-rows.md: $(SECTIONS)
	@rm -f "$@"
	@for section in $(SECTIONS); do \
	  pandoc --to=markdown+fancy_lists \
		--highlight=zenburn \
		--template=../.templates/status-report-row.md.template \
		--wrap=none \
		--metadata="audience:$(AUDIENCE)" \
		--metadata="section:$$(echo $$section | sed 's|./sections/||')" \
		"$$section" >> "$@"; \
	done
